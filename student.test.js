const http = require('http');
const rp = require('request-promise');
const apiUrl = 'https://roulette-api.nowakowski-arkadiusz.com';

// Helper to make the code shorter
const post = (path, hashname = '', body = {}) => rp({
    method: 'POST',
    uri: apiUrl + path,
    body: body,
    json: true,
    headers: {'Authorization': hashname}
});

// Helper to make the code shorter
const get = (path, hashname = '') => rp({
    method: 'GET',
    uri: apiUrl + path,
    json: true,
    headers: {'Authorization': hashname}
});
/** 1 **/
test('Bet on reds should double the number of chips if number 12 is spinned', () => {
    let hashname;
    return post('/players')
        .then((response) => {
            hashname = response.hashname;
            return post('/bets/red', hashname, { chips: 100 }); // a bet
        })
        .then((response) => post('/spin/' + 12, hashname)) // spin the wheel
        .then((response) => get('/chips', hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(200));
});
/** 2 **/
test('Bet on black should double the number of chips if number 11 is spinned', () => {
    let hashname;
    return post('/players')
        .then((response) => {
            hashname = response.hashname;
            return post('/bets/black', hashname, { chips: 100 }); // a bet
        })
        .then((response) => post('/spin/' + 11, hashname)) // spin the wheel
        .then((response) => get('/chips', hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(200));
});
/** 3 **/
test('Bet on even should return 0 chips if number 0 is spinned', () => {
    let hashname;
    return post('/players')
        .then((response) => {
            hashname = response.hashname;
            return post('/bets/even', hashname, { chips: 100 }); // a bet
        })
        .then((response) => post('/spin/' + 0, hashname)) // spin the wheel
        .then((response) => get('/chips', hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(0));
});
/** 4 **/
test('Bet on straight should multiply by 35 the number of chips if number 4 is spinned', () => {
    let hashname;
    return post('/players')
        .then((response) => {
            hashname = response.hashname;
            return post('/bets/straight/' + 4, hashname, { chips: 100 }); // a bet
        })
        .then((response) => post('/spin/' + 4, hashname)) // spin the wheel
        .then((response) => get('/chips', hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(3600));
});
/** 5 **/
test('Bet on high numbers should double the number of chips if number 19 is spinned', () => {
    let hashname;
    return post('/players')
        .then((response) => {
            hashname = response.hashname;
            return post('/bets/high/' + 4, hashname, { chips: 100 }); // a bet
        })
        .then((response) => post('/spin/' + 19, hashname)) // spin the wheel
        .then((response) => get('/chips', hashname)) // checking the number of chips
        .then((response) => expect(response.chips).toEqual(3600));
});
/** 6 **/
test.each([31, 32, 34, 35])(
    'Bet on corner 31-32-34-35 should multiply by 9 the number of chips if number %d is spinned',
    (number) =>
        post('/players')
            .then((response) => {
                hashname = response.hashname;
                return post('/bets/corner/31-32-34-35', hashname, { chips: 100 }); // a bet
            })
            .then((response) => post('/spin/' + number, hashname)) // spin the wheel
            .then((response) => get('/chips', hashname)) // checking the number of chips
            .then((response) => expect(response.chips).toEqual(900))
);
/** 7 test failed for bets/line/ example below**/
test.each([1, 2, 3, 4, 5, 6])('Bet on line numbers should multiply the number of chips if number %d is spinned',
    (number) =>
        post('/players')
            .then((response) => {
                hashname = response.hashname;
                return post('/bets/line/1-2-3-4-5-6', hashname, {chips: 100}); // a bet
            })
            .then((response) => post('/spin/' + number, hashname)) // spin the wheel
            .then((response) => get('/chips', hashname)) // checking the number of chips
            .then((response) => expect(response.chips).toEqual(600))
);
/** 8 for /split/ 1200 instead of 1800**/
test.each([0,1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18,
    19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34])
('Bet on split should multiply the number of chips if number %d is spinned',
    (number) =>
        post('/players')
            .then((response) => {
                hashname = response.hashname;
                let v = number + 1;
                return post('/bets/split/' +number + '-' + v, hashname, {chips: 100}); // a bet
            })
            .then((response) => post('/spin/' + number, hashname)) // spin the wheel
            .then((response) => get('/chips', hashname)) // checking the number of chips
            .then((response) => expect(response.chips).toEqual(1800))
);

/** RED CASE **/

//fail for 12
// test.each([1,3,5,7,9,12,14,16,18,21,23,25,27,28,30,32,34,36])('Bet on reds should double the number of chips if number %d is spinned', (number) => {
//     return post('/players')
//         .then(response => {
//             hashname = response.hashname;
//             return post('/bets/red', hashname, { chips: 100 }); // a bet
//         })
//         .then(response => post('/spin/' + number, hashname)) // splin the wheel
//         .then(response => get('/chips', hashname)) // checking the number of chips
//         .then(response => expect(response.chips).toEqual(200))
// });

// test.each([2, 4, 6, 8, 10, 11, 13, 15, 17, 19, 20, 22, 24, 26, 29, 31, 33, 35])('Bet on red should return 0 chips if number %d is spinned', (number) => {
//     return post('/players')
//         .then(response => {
//             hashname = response.hashname;
//             return post('/bets/red', hashname, {chips: 100}); // a bet
//         })
//         .then(response => post('/spin/' + number, hashname)) // splin the wheel
//         .then(response => get('/chips', hashname)) // checking the number of chips
//         .then(response => expect(response.chips).toEqual(0))
// });

// test('Bet on reds should return 0 chips if number 0 is spinned', () => {
//     let hashname;
//     return post('/players')
//         .then((response) => {
//             hashname = response.hashname;
//             return post('/bets/red', hashname, { chips: 100 }); // a bet
//         })
//         .then((response) => post('/spin/' + 0, hashname)) // spin the wheel
//         .then((response) => get('/chips', hashname)) // checking the number of chips
//         .then((response) => expect(response.chips).toEqual(0));
// });

/** BLACK CASE **/

// test.each([1, 3, 5, 7, 9, 12, 14, 16, 18, 21, 23, 25, 27, 28, 30, 32, 34, 36])('Bet on black should return 0 chips if number %d is spinned', (number) => {
//     return post('/players')
//         .then(response => {
//             hashname = response.hashname;
//             return post('/bets/black', hashname, {chips: 100}); // a bet
//         })
//         .then(response => post('/spin/' + number, hashname)) // splin the wheel
//         .then(response => get('/chips', hashname)) // checking the number of chips
//         .then(response => expect(response.chips).toEqual(0))
// });

//fail for 11
// test.each([2, 4, 6, 8, 10, 11, 13, 15, 17, 19, 20, 22, 24, 26, 29, 31, 33, 35])('Bet on black should double the number of chips if number %d is spinned', (number) => {
//     return post('/players')
//         .then(response => {
//             hashname = response.hashname;
//             return post('/bets/black', hashname, {chips: 100}); // a bet
//         })
//         .then(response => post('/spin/' + number, hashname)) // splin the wheel
//         .then(response => get('/chips', hashname)) // checking the number of chips
//         .then(response => expect(response.chips).toEqual(200))
// });

// test('Bet on black should return 0 chips if number 0 is spinned', () => {
//     let hashname;
//     return post('/players')
//         .then((response) => {
//             hashname = response.hashname;
//             return post('/bets/black', hashname, { chips: 100 }); // a bet
//         })
//         .then((response) => post('/spin/' + 0, hashname)) // spin the wheel
//         .then((response) => get('/chips', hashname)) // checking the number of chips
//         .then((response) => expect(response.chips).toEqual(0));
// });
/** ODD CASE **/

// test.each([1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35])('Bet on odds should double the number of chips if number %d is spinned',
//   (number) =>
//     post('/players')
//       .then((response) => {
//         hashname = response.hashname;
//         return post('/bets/odd', hashname, { chips: 100 }); // a bet
//       })
//       .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//       .then((response) => get('/chips', hashname)) // checking the number of chips
//       .then((response) => expect(response.chips).toEqual(200))
// );

// test.each([2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36])(
//   'Bet on odd should return 0 chips if number %d is spinned',
//   (number) =>
//     post('/players')
//       .then((response) => {
//         hashname = response.hashname;
//         return post('/bets/odd', hashname, { chips: 100 }); // a bet
//       })
//       .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//       .then((response) => get('/chips', hashname)) // checking the number of chips
//       .then((response) => expect(response.chips).toEqual(0))
// );

// test('Bet on odd should return 0 chips if number 0 is spinned', () => {
//     let hashname;
//     return post('/players')
//         .then((response) => {
//             hashname = response.hashname;
//             return post('/bets/odd', hashname, { chips: 100 }); // a bet
//         })
//         .then((response) => post('/spin/' + 0, hashname)) // spin the wheel
//         .then((response) => get('/chips', hashname)) // checking the number of chips
//         .then((response) => expect(response.chips).toEqual(0));
// });

/** EVEN CASE **/

// test.each([1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35])('Bet on even should return 0 chips if number %d is spinned',
//     (number) =>
//         post('/players')
//             .then((response) => {
//                 hashname = response.hashname;
//                 return post('/bets/even', hashname, { chips: 100 }); // a bet
//             })
//             .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//             .then((response) => get('/chips', hashname)) // checking the number of chips
//             .then((response) => expect(response.chips).toEqual(0))
// );
// test.each([1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35])('Bet on even should double the number of chips if number %d is spinned',
//     (number) =>
//         post('/players')
//             .then((response) => {
//                 hashname = response.hashname;
//                 return post('/bets/even', hashname, { chips: 100 }); // a bet
//             })
//             .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//             .then((response) => get('/chips', hashname)) // checking the number of chips
//             .then((response) => expect(response.chips).toEqual(0))
// );

//fail for 0 if even
// test('Bet on even should return 0 chips if number 0 is spinned', () => {
//     let hashname;
//     return post('/players')
//         .then((response) => {
//             hashname = response.hashname;
//             return post('/bets/even', hashname, { chips: 100 }); // a bet
//         })
//         .then((response) => post('/spin/' + 0, hashname)) // spin the wheel
//         .then((response) => get('/chips', hashname)) // checking the number of chips
//         .then((response) => expect(response.chips).toEqual(0));
// });


/** STRAIGHT CASE **/
//fail number 4
// test.each([0,1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18,
//     19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36])('Bet on strait should x35 the number of chips if number %d is spinned',
//     (number) =>
//         post('/players')
//             .then((response) => {
//                 hashname = response.hashname;
//                 return post('/bets/straight/' + number, hashname, {chips: 100}); // a bet
//             })
//             .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//             .then((response) => get('/chips', hashname)) // checking the number of chips
//             .then((response) => expect(response.chips).toEqual(3600))
// );

/** LOW NUMBER CASE **/

// test.each([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18])('Bet on low numbers should double the number of chips if number %d is spinned',
//     (number) =>
//         post('/players')
//             .then((response) => {
//                 hashname = response.hashname;
//                 return post('/bets/low', hashname, {chips: 100}); // a bet
//             })
//             .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//             .then((response) => get('/chips', hashname)) // checking the number of chips
//             .then((response) => expect(response.chips).toEqual(200))
// );

// test.each([19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36])('Bet on low numbers should return 0 chips if number %d is spinned',
//     (number) =>
//         post('/players')
//             .then((response) => {
//                 hashname = response.hashname;
//                 return post('/bets/low', hashname, {chips: 100}); // a bet
//             })
//             .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//             .then((response) => get('/chips', hashname)) // checking the number of chips
//             .then((response) => expect(response.chips).toEqual(0))
// );

/** HIGH NUMBER CASE **/
// fail for 19
// test.each([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18])('Bet on high numbers should double the number of chips if number %d is spinned',
//     (number) =>
//         post('/players')
//             .then((response) => {
//                 hashname = response.hashname;
//                 return post('/bets/high', hashname, {chips: 100}); // a bet
//             })
//             .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//             .then((response) => get('/chips', hashname)) // checking the number of chips
//             .then((response) => expect(response.chips).toEqual(0))
// );
// test.each([19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36])('Bet on high numbers should return 0 chips if number %d is spinned',
//     (number) =>
//         post('/players')
//             .then((response) => {
//                 hashname = response.hashname;
//                 return post('/bets/high', hashname, {chips: 100}); // a bet
//             })
//             .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//             .then((response) => get('/chips', hashname)) // checking the number of chips
//             .then((response) => expect(response.chips).toEqual(200))
// );

/** LINE CASE **/
//fail for 6
// test.each([1, 2, 3, 4, 5, 6])('Bet on line numbers should multiply the number of chips if number %d is spinned',
// //     (number) =>
// //         post('/players')
// //             .then((response) => {
// //                 hashname = response.hashname;
// //                 return post('/bets/line/1-2-3-4-5-6', hashname, {chips: 100}); // a bet
// //             })
// //             .then((response) => post('/spin/' + number, hashname)) // spin the wheel
// //             .then((response) => get('/chips', hashname)) // checking the number of chips
// //             .then((response) => expect(response.chips).toEqual(500))
// // );

// test.each([1,4,7,10,13,16,19,22,25,28,31,34])('Bet on column 1 should multiply x2 the number of chips if number %d is spinned',
//     (number) =>
//         post('/players')
//             .then((response) => {
//                 hashname = response.hashname;
//                 return post('/bets/column/1', hashname, {chips: 100}); // a bet
//             })
//             .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//             .then((response) => get('/chips', hashname)) // checking the number of chips
//             .then((response) => expect(response.chips).toEqual(300))
// );

// test.each([2,5,8,11,14,17,20,23,26,29,32,35])('Bet on column 2 should multiply x2 the number of chips if number %d is spinned',
//     (number) =>
//         post('/players')
//             .then((response) => {
//                 hashname = response.hashname;
//                 return post('/bets/column/2', hashname, {chips: 100}); // a bet
//             })
//             .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//             .then((response) => get('/chips', hashname)) // checking the number of chips
//             .then((response) => expect(response.chips).toEqual(300))
// );

// test.each([3,6,9,12,15,18,21,24,27,30,33,36])('Bet on column 3 should multiply x2 the number of chips if number %d is spinned',
//     (number) =>
//         post('/players')
//             .then((response) => {
//                 hashname = response.hashname;
//                 return post('/bets/column/3', hashname, {chips: 100}); // a bet
//             })
//             .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//             .then((response) => get('/chips', hashname)) // checking the number of chips
//             .then((response) => expect(response.chips).toEqual(300))
// );

/** CORNER CASE **/

// test.each([2,3,5,6])('Bet on column 2 should multiply x2 the number of chips if number %s is spinned',
//     (number) =>
//         post('/players')
//             .then((response) => {
//                 hashname = response.hashname;
//                 return post('/bets/corner/2-3-5-6', hashname, {chips: 100}); // a bet
//             })
//             .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//             .then((response) => get('/chips', hashname)) // checking the number of chips
//             .then((response) => expect(response.chips).toEqual(900))
// );
// test.each([1, 2, 4, 5])(
//   'Bet on corner 1-2-4-5 should multiply by 9 the number of chips if number %d is spinned',
//   (number) =>
//     post('/players')
//       .then((response) => {
//         hashname = response.hashname;
//         return post('/bets/corner/1-2-4-5', hashname, { chips: 100 }); // a bet
//       })
//       .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//       .then((response) => get('/chips', hashname)) // checking the number of chips
//       .then((response) => expect(response.chips).toEqual(900))
// );
//
// test.each([4, 5, 7, 8])(
//   'Bet on corner 4-5-7-8 should multiply by 9 the number of chips if number %d is spinned',
//   (number) =>
//     post('/players')
//       .then((response) => {
//         hashname = response.hashname;
//         return post('/bets/corner/4-5-7-8', hashname, { chips: 100 }); // a bet
//       })
//       .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//       .then((response) => get('/chips', hashname)) // checking the number of chips
//       .then((response) => expect(response.chips).toEqual(900))
// );
//
// test.each([5, 6, 8, 9])(
//   'Bet on corner 5-6-8-9 should multiply by 9 the number of chips if number %d is spinned',
//   (number) =>
//     post('/players')
//       .then((response) => {
//         hashname = response.hashname;
//         return post('/bets/corner/5-6-8-9', hashname, { chips: 100 }); // a bet
//       })
//       .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//       .then((response) => get('/chips', hashname)) // checking the number of chips
//       .then((response) => expect(response.chips).toEqual(900))
// );
//
// test.each([7, 8, 10, 11])(
//   'Bet on corner 7-8-10-11 should multiply by 9 the number of chips if number %d is spinned',
//   (number) =>
//     post('/players')
//       .then((response) => {
//         hashname = response.hashname;
//         return post('/bets/corner/7-8-10-11', hashname, { chips: 100 }); // a bet
//       })
//       .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//       .then((response) => get('/chips', hashname)) // checking the number of chips
//       .then((response) => expect(response.chips).toEqual(900))
// );
//
// test.each([8, 9, 11, 12])(
//   'Bet on corner 8-9-11-12 should multiply by 9 the number of chips if number %d is spinned',
//   (number) =>
//     post('/players')
//       .then((response) => {
//         hashname = response.hashname;
//         return post('/bets/corner/8-9-11-12', hashname, { chips: 100 }); // a bet
//       })
//       .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//       .then((response) => get('/chips', hashname)) // checking the number of chips
//       .then((response) => expect(response.chips).toEqual(900))
// );
//
// test.each([10, 11, 13, 14])(
//   'Bet on corner 10-11-13-14 should multiply by 9 the number of chips if number %d is spinned',
//   (number) =>
//     post('/players')
//       .then((response) => {
//         hashname = response.hashname;
//         return post('/bets/corner/10-11-13-14', hashname, { chips: 100 }); // a bet
//       })
//       .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//       .then((response) => get('/chips', hashname)) // checking the number of chips
//       .then((response) => expect(response.chips).toEqual(900))
// );
//
// test.each([11, 12, 14, 15])(
//   'Bet on corner 11-12-14-15 should multiply by 9 the number of chips if number %d is spinned',
//   (number) =>
//     post('/players')
//       .then((response) => {
//         hashname = response.hashname;
//         return post('/bets/corner/11-12-14-15', hashname, { chips: 100 }); // a bet
//       })
//       .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//       .then((response) => get('/chips', hashname)) // checking the number of chips
//       .then((response) => expect(response.chips).toEqual(900))
// );
//
// test.each([13, 14, 16, 17])(
//   'Bet on corner 13-14-16-17 should multiply by 9 the number of chips if number %d is spinned',
//   (number) =>
//     post('/players')
//       .then((response) => {
//         hashname = response.hashname;
//         return post('/bets/corner/13-14-16-17', hashname, { chips: 100 }); // a bet
//       })
//       .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//       .then((response) => get('/chips', hashname)) // checking the number of chips
//       .then((response) => expect(response.chips).toEqual(900))
// );
//
// test.each([14, 15, 17, 18])(
//   'Bet on corner 14-15-17-18 should multiply by 9 the number of chips if number %d is spinned',
//   (number) =>
//     post('/players')
//       .then((response) => {
//         hashname = response.hashname;
//         return post('/bets/corner/14-15-17-18', hashname, { chips: 100 }); // a bet
//       })
//       .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//       .then((response) => get('/chips', hashname)) // checking the number of chips
//       .then((response) => expect(response.chips).toEqual(900))
// );
//
// test.each([16, 17, 19, 20])(
//   'Bet on corner 16-17-19-20 should multiply by 9 the number of chips if number %d is spinned',
//   (number) =>
//     post('/players')
//       .then((response) => {
//         hashname = response.hashname;
//         return post('/bets/corner/16-17-19-20', hashname, { chips: 100 }); // a bet
//       })
//       .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//       .then((response) => get('/chips', hashname)) // checking the number of chips
//       .then((response) => expect(response.chips).toEqual(900))
// );
//
// test.each([17, 18, 20, 21])(
//   'Bet on corner 17-18-20-21 should multiply by 9 the number of chips if number %d is spinned',
//   (number) =>
//     post('/players')
//       .then((response) => {
//         hashname = response.hashname;
//         return post('/bets/corner/17-18-20-21', hashname, { chips: 100 }); // a bet
//       })
//       .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//       .then((response) => get('/chips', hashname)) // checking the number of chips
//       .then((response) => expect(response.chips).toEqual(900))
// );
//
// test.each([19, 20, 22, 23])(
//   'Bet on corner 19-20-22-23 should multiply by 9 the number of chips if number %d is spinned',
//   (number) =>
//     post('/players')
//       .then((response) => {
//         hashname = response.hashname;
//         return post('/bets/corner/19-20-22-23', hashname, { chips: 100 }); // a bet
//       })
//       .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//       .then((response) => get('/chips', hashname)) // checking the number of chips
//       .then((response) => expect(response.chips).toEqual(900))
// );
//
// test.each([20, 21, 23, 24])(
//   'Bet on corner 20-21-23-24 should multiply by 9 the number of chips if number %d is spinned',
//   (number) =>
//     post('/players')
//       .then((response) => {
//         hashname = response.hashname;
//         return post('/bets/corner/20-21-23-24', hashname, { chips: 100 }); // a bet
//       })
//       .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//       .then((response) => get('/chips', hashname)) // checking the number of chips
//       .then((response) => expect(response.chips).toEqual(900))
// );
//
// test.each([22, 23, 25, 26])(
//   'Bet on corner 22-23-25-26 should multiply by 9 the number of chips if number %d is spinned',
//   (number) =>
//     post('/players')
//       .then((response) => {
//         hashname = response.hashname;
//         return post('/bets/corner/22-23-25-26', hashname, { chips: 100 }); // a bet
//       })
//       .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//       .then((response) => get('/chips', hashname)) // checking the number of chips
//       .then((response) => expect(response.chips).toEqual(900))
// );
//
// test.each([23, 24, 26, 27])(
//   'Bet on corner 23-24-26-27 should multiply by 9 the number of chips if number %d is spinned',
//   (number) =>
//     post('/players')
//       .then((response) => {
//         hashname = response.hashname;
//         return post('/bets/corner/23-24-26-27', hashname, { chips: 100 }); // a bet
//       })
//       .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//       .then((response) => get('/chips', hashname)) // checking the number of chips
//       .then((response) => expect(response.chips).toEqual(900))
// );
//
// test.each([25, 26, 28, 29])(
//   'Bet on corner 25-26-28-29 should multiply by 9 the number of chips if number %d is spinned',
//   (number) =>
//     post('/players')
//       .then((response) => {
//         hashname = response.hashname;
//         return post('/bets/corner/25-26-28-29', hashname, { chips: 100 }); // a bet
//       })
//       .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//       .then((response) => get('/chips', hashname)) // checking the number of chips
//       .then((response) => expect(response.chips).toEqual(900))
// );
//
// test.each([26, 27, 29, 30])(
//   'Bet on corner 26-27-29-30 should multiply by 9 the number of chips if number %d is spinned',
//   (number) =>
//     post('/players')
//       .then((response) => {
//         hashname = response.hashname;
//         return post('/bets/corner/26-27-29-30', hashname, { chips: 100 }); // a bet
//       })
//       .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//       .then((response) => get('/chips', hashname)) // checking the number of chips
//       .then((response) => expect(response.chips).toEqual(900))
// );
//
// test.each([28, 29, 31, 32])(
//   'Bet on corner 28-29-31-32 should multiply by 9 the number of chips if number %d is spinned',
//   (number) =>
//     post('/players')
//       .then((response) => {
//         hashname = response.hashname;
//         return post('/bets/corner/28-29-31-32', hashname, { chips: 100 }); // a bet
//       })
//       .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//       .then((response) => get('/chips', hashname)) // checking the number of chips
//       .then((response) => expect(response.chips).toEqual(900))
// );
//
// test.each([29, 30, 32, 33])(
//   'Bet on corner 29-30-32-33 should multiply by 9 the number of chips if number %d is spinned',
//   (number) =>
//     post('/players')
//       .then((response) => {
//         hashname = response.hashname;
//         return post('/bets/corner/29-30-32-33', hashname, { chips: 100 }); // a bet
//       })
//       .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//       .then((response) => get('/chips', hashname)) // checking the number of chips
//       .then((response) => expect(response.chips).toEqual(900))
// );
//
// test.each([32, 33, 35, 36])(
//   'Bet on corner 32-33-35-36 should multiply by 9 the number of chips if number %d is spinned',
//   (number) =>
//     post('/players')
//       .then((response) => {
//         hashname = response.hashname;
//         return post('/bets/corner/32-33-35-36', hashname, { chips: 100 }); // a bet
//       })
//       .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//       .then((response) => get('/chips', hashname)) // checking the number of chips
//       .then((response) => expect(response.chips).toEqual(900))
// );

/** DOZEN CASE **/
//
// test.each([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])(
//     'Bet on dozen 1 should multiply by 3 the number of chips if number %d is spinned',
//     (number) =>
//         post('/players')
//             .then((response) => {
//                 hashname = response.hashname;
//                 return post('/bets/dozen/1', hashname, {chips: 100}); // a bet
//             })
//             .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//             .then((response) => get('/chips', hashname)) // checking the number of chips
//             .then((response) => expect(response.chips).toEqual(300))
// );
// test.each([13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24,])(
//     'Bet on dozen 2 should multiply by 3 the number of chips if number %d is spinned',
//     (number) =>
//         post('/players')
//             .then((response) => {
//                 hashname = response.hashname;
//                 return post('/bets/dozen/2', hashname, {chips: 100}); // a bet
//             })
//             .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//             .then((response) => get('/chips', hashname)) // checking the number of chips
//             .then((response) => expect(response.chips).toEqual(300))
// );

// test.each([25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36])(
//     'Bet on dozen 3 should multiply by 3 the number of chips if number %d is spinned',
//     (number) =>
//         post('/players')
//             .then((response) => {
//                 hashname = response.hashname;
//                 return post('/bets/dozen/3', hashname, {chips: 100}); // a bet
//             })
//             .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//             .then((response) => get('/chips', hashname)) // checking the number of chips
//             .then((response) => expect(response.chips).toEqual(300))
// );

/** STREET CASE **/
//test.each([1,2,3])
// ('Bet on street should multiply the number of chips if number %d is spinned',
//     (number) =>
//         post('/players')
//             .then((response) => {
//                 hashname = response.hashname;
//                 let v = number + 1;
//                 return post('/bets/street/1-2-3', hashname, {chips: 100}); // a bet
//             })
//             .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//             .then((response) => get('/chips', hashname)) // checking the number of chips
//             .then((response) => expect(response.chips).toEqual(1200))
// );
// test.each([4,5,6])
// ('Bet on street should multiply the number of chips if number %d is spinned',
//     (number) =>
//         post('/players')
//             .then((response) => {
//                 hashname = response.hashname;
//                 let v = number + 1;
//                 return post('/bets/street/4-5-6', hashname, {chips: 100}); // a bet
//             })
//             .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//             .then((response) => get('/chips', hashname)) // checking the number of chips
//             .then((response) => expect(response.chips).toEqual(1200))
// );
// test.each([7,8,9])
// ('Bet on street should multiply the number of chips if number %d is spinned',
//     (number) =>
//         post('/players')
//             .then((response) => {
//                 hashname = response.hashname;
//                 let v = number + 1;
//                 return post('/bets/street/7-8-9', hashname, {chips: 100}); // a bet
//             })
//             .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//             .then((response) => get('/chips', hashname)) // checking the number of chips
//             .then((response) => expect(response.chips).toEqual(1200))
// );
// test.each([10,11,12])
// ('Bet on street should multiply the number of chips if number %d is spinned',
//     (number) =>
//         post('/players')
//             .then((response) => {
//                 hashname = response.hashname;
//                 let v = number + 1;
//                 return post('/bets/street/10-11-12', hashname, {chips: 100}); // a bet
//             })
//             .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//             .then((response) => get('/chips', hashname)) // checking the number of chips
//             .then((response) => expect(response.chips).toEqual(1200))
// );
// test.each([13,14,15])
// ('Bet on street should multiply the number of chips if number %d is spinned',
//     (number) =>
//         post('/players')
//             .then((response) => {
//                 hashname = response.hashname;
//                 let v = number + 1;
//                 return post('/bets/street/13-14-15', hashname, {chips: 100}); // a bet
//             })
//             .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//             .then((response) => get('/chips', hashname)) // checking the number of chips
//             .then((response) => expect(response.chips).toEqual(1200))
// );
// test.each([16,17,18])
// ('Bet on street should multiply the number of chips if number %d is spinned',
//     (number) =>
//         post('/players')
//             .then((response) => {
//                 hashname = response.hashname;
//                 let v = number + 1;
//                 return post('/bets/street/16-17-18', hashname, {chips: 100}); // a bet
//             })
//             .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//             .then((response) => get('/chips', hashname)) // checking the number of chips
//             .then((response) => expect(response.chips).toEqual(1200))
// );
// test.each([19,20,21])
// ('Bet on street should multiply the number of chips if number %d is spinned',
//     (number) =>
//         post('/players')
//             .then((response) => {
//                 hashname = response.hashname;
//                 let v = number + 1;
//                 return post('/bets/street/19-20-21', hashname, {chips: 100}); // a bet
//             })
//             .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//             .then((response) => get('/chips', hashname)) // checking the number of chips
//             .then((response) => expect(response.chips).toEqual(1200))
// );
// test.each([22,23,24])
// ('Bet on street should multiply the number of chips if number %d is spinned',
//     (number) =>
//         post('/players')
//             .then((response) => {
//                 hashname = response.hashname;
//                 let v = number + 1;
//                 return post('/bets/street/22-23-24', hashname, {chips: 100}); // a bet
//             })
//             .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//             .then((response) => get('/chips', hashname)) // checking the number of chips
//             .then((response) => expect(response.chips).toEqual(1200))
// );
// test.each([25,26,27])
// ('Bet on street should multiply the number of chips if number %d is spinned',
//     (number) =>
//         post('/players')
//             .then((response) => {
//                 hashname = response.hashname;
//                 let v = number + 1;
//                 return post('/bets/street/25-26-27', hashname, {chips: 100}); // a bet
//             })
//             .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//             .then((response) => get('/chips', hashname)) // checking the number of chips
//             .then((response) => expect(response.chips).toEqual(1200))
// );
// test.each([28,29,30])
// ('Bet on street should multiply the number of chips if number %d is spinned',
//     (number) =>
//         post('/players')
//             .then((response) => {
//                 hashname = response.hashname;
//                 let v = number + 1;
//                 return post('/bets/street/28-29-30', hashname, {chips: 100}); // a bet
//             })
//             .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//             .then((response) => get('/chips', hashname)) // checking the number of chips
//             .then((response) => expect(response.chips).toEqual(1200))
// );
// test.each([31,32,33])
// ('Bet on street should multiply the number of chips if number %d is spinned',
//     (number) =>
//         post('/players')
//             .then((response) => {
//                 hashname = response.hashname;
//                 let v = number + 1;
//                 return post('/bets/street/31-32-33', hashname, {chips: 100}); // a bet
//             })
//             .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//             .then((response) => get('/chips', hashname)) // checking the number of chips
//             .then((response) => expect(response.chips).toEqual(1200))
// );
// test.each([34,35,36])
// ('Bet on street should multiply the number of chips if number %d is spinned',
//     (number) =>
//         post('/players')
//             .then((response) => {
//                 hashname = response.hashname;
//                 let v = number + 1;
//                 return post('/bets/street/34-35-36', hashname, {chips: 100}); // a bet
//             })
//             .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//             .then((response) => get('/chips', hashname)) // checking the number of chips
//             .then((response) => expect(response.chips).toEqual(1200))
// );
// test.each([0,1,2])
// ('Bet on street should multiply the number of chips if number %d is spinned',
//     (number) =>
//         post('/players')
//             .then((response) => {
//                 hashname = response.hashname;
//                 let v = number + 1;
//                 return post('/bets/street/0-1-2', hashname, {chips: 100}); // a bet
//             })
//             .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//             .then((response) => get('/chips', hashname)) // checking the number of chips
//             .then((response) => expect(response.chips).toEqual(1200))
// );
// test.each([0,2,3])
// ('Bet on street should multiply the number of chips if number %d is spinned',
//     (number) =>
//         post('/players')
//             .then((response) => {
//                 hashname = response.hashname;
//                 let v = number + 1;
//                 return post('/bets/street/0-2-3', hashname, {chips: 100}); // a bet
//             })
//             .then((response) => post('/spin/' + number, hashname)) // spin the wheel
//             .then((response) => get('/chips', hashname)) // checking the number of chips
//             .then((response) => expect(response.chips).toEqual(1200))
// );
